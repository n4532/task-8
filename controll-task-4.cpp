//#include <iostream>
//
//using namespace std;
//
//double rootDegree(double, double, unsigned, unsigned&);
//void createTable(double, double, double, double, unsigned, unsigned&);
//
//int main()
//{
//	double begin = 6, end = 8, step = 0.25, accuracy = 0.01;
//	unsigned degree = 2;
//	cout << " Enter a,b,end (type real), eps (epsilon) and degree \n";
//	cin >> begin;
//	cin >> end;
//	cin >> step;
//	cin >> accuracy;
//	
//	system("cls");
//	cout << "\n  Table of function's values the root of " << degree << " - power from x" << endl;
//	unsigned iterations = 0;
//	
//	createTable(begin, end, step, accuracy, degree, iterations);
//
//	cout << "  The maximum number of iteration is equal to " << iterations << "\n";
//	cout << "  P r e s s   a n y   k e y   t o  E X I T . . . ";
//	system("pause");
//	return 0;
//}
//
//void createTable(double begin, double end, double step, double accuracy, unsigned degree, unsigned& iterations)
//{
//	cout << " \xC9";
//	cout.fill('\xCD');
//	cout.width(6);
//	cout << '\xCB';
//	cout.width(12);
//	cout << "\xCB";
//	cout.width(12);
//	cout << "\xCB";
//	cout.width(16);
//	cout << "\xCB";
//	cout.width(15);
//	cout << "\xBB\n";
//	cout << " \xBA  x  \xBA     f     \xBA     f~    \xBA   Accurancy   \xBA    Count    \xBA\n";
//	cout << " \xCC";
//	cout.width(6);
//	cout << "\xCE";
//	cout.width(12);
//	cout << "\xCE";
//	cout.width(12);
//	cout << "\xCE";
//	cout.width(16);
//	cout << "\xCE";
//	cout.width(15);
//	cout << "\xB9\n";
//	cout.setf(ios::fixed, ios::floatfield);
//	cout.fill(' ');
//
//	for (double x = begin; x <= end; x += step)
//	{
//		unsigned& iteration = iterations;
//		double y = 1 / rootDegree(x, accuracy, degree, iteration);
//		double y1 = 1 / sqrt(x);
//		double accurancy = y1 - y;
//		if (iteration > iterations) iterations = iteration;
//
//		cout << " \xBA";
//		cout.width(4);
//		cout.precision(1);
//		cout << x; // x
//		cout << " \xBA";
//		cout.width(10);
//		cout.precision(6);
//		cout << y1; // f
//		cout << " \xBA";
//		cout.width(10);
//		cout.precision(6);
//		cout << y; // f~
//		cout << " \xBA";
//		cout.width(14);
//		cout.precision(6);
//		cout << accurancy; // Accurancy
//		cout << " \xBA";
//		cout.width(12);
//		cout.precision(6);
//		cout << iteration; // count
//		cout << " \xBA\n";
//	}
//	cout.fill('\xCD');
//	cout << " \xC8";
//	cout.fill('\xCD');
//	cout.width(6);
//	cout << "\xCA";
//	cout.fill('\xCD');
//	cout.width(12);
//	cout << "\xCA";
//	cout.fill('\xCD');
//	cout.width(12);
//	cout << "\xCA";
//	cout.fill('\xCD');
//	cout.width(16);
//	cout << "\xCA";
//	cout.fill('\xCD');
//	cout.width(15);
//	cout << "\xBC\n";
//}
//
//double rootDegree(double x, double accuracy, unsigned degree, unsigned& countOfInterations)
//{
//	double previous = x, following = ((degree - 1) * previous + x / pow(previous, degree)) / degree;
//	countOfInterations = 0;
//
//	while (fabs(previous - following) > accuracy)
//	{
//		previous = following;
//		following = ((degree - 1) * previous + x / pow(previous, degree - 1)) / degree;
//		countOfInterations++;
//	}
//
//	return following;
//}


#include <iostream>
#include <fstream>

using namespace std;

double rootDegree(double, double, unsigned, unsigned&);
void createTable(double, double, double, double, unsigned, unsigned&);

int main()
{
	double begin = 6, end = 8, step = 0.25, accuracy = 0.01;
	unsigned degree = 2;
	cout << " Enter a,b,end (type real), eps (epsilon) and degree \n";
	cin >> begin;
	cin >> end;
	cin >> step;
	cin >> accuracy;

	system("cls");
	cout << "\n  Table of function's values the root of " << degree << " - power from x" << endl;
	unsigned iterations = 0;

	createTable(begin, end, step, accuracy, degree, iterations);

	cout << "  The maximum number of iteration is equal to " << iterations << "\n";
	cout << "  P r e s s   a n y   k e y   t o  E X I T . . . ";
	system("pause");
	return 0;
}

void createTable(double begin, double end, double step, double accuracy, unsigned degree, unsigned& iterations)
{
	ofstream fin;
	fin.open("text.txt");

	if (!fin.is_open()) {
		cout << "Could not open the file";
		return;
	}

	fin << " \xC9";
	fin.fill('\xCD');
	fin.width(6);
	fin << '\xCB';
	fin.width(12);
	fin << "\xCB";
	fin.width(12);
	fin << "\xCB";
	fin.width(16);
	fin << "\xCB";
	fin.width(15);
	fin << "\xBB\n";
	fin << " \xBA  x  \xBA     f     \xBA     f~    \xBA   Accurancy   \xBA    Count    \xBA\n";
	fin << " \xCC";
	fin.width(6);
	fin << "\xCE";
	fin.width(12);
	fin << "\xCE";
	fin.width(12);
	fin << "\xCE";
	fin.width(16);
	fin << "\xCE";
	fin.width(15);
	fin << "\xB9\n";
	fin.setf(ios::fixed, ios::floatfield);
	fin.fill(' ');

	for (double x = begin; x <= end; x += step)
	{
		unsigned& iteration = iterations;
		double y = 1 / rootDegree(x, accuracy, degree, iteration);
		double y1 = 1 / sqrt(x);
		double accurancy = y1 - y;
		if (iteration > iterations) iterations = iteration;

		fin << " \xBA";
		fin.width(4);
		fin.precision(1);
		fin << x; // x
		fin << " \xBA";
		fin.width(10);
		fin.precision(6);
		fin << y1; // f
		fin << " \xBA";
		fin.width(10);
		fin.precision(6);
		fin << y; // f~
		fin << " \xBA";
		fin.width(14);
		fin.precision(6);
		fin << accurancy; // Accurancy
		fin << " \xBA";
		fin.width(12);
		fin.precision(6);
		fin << iteration; // count
		fin << " \xBA\n";
	}
	fin.fill('\xCD');
	fin << " \xC8";
	fin.fill('\xCD');
	fin.width(6);
	fin << "\xCA";
	fin.fill('\xCD');
	fin.width(12);
	fin << "\xCA";
	fin.fill('\xCD');
	fin.width(12);
	fin << "\xCA";
	fin.fill('\xCD');
	fin.width(16);
	fin << "\xCA";
	fin.fill('\xCD');
	fin.width(15);
	fin << "\xBC\n";

	fin.close();
}

double rootDegree(double x, double accuracy, unsigned degree, unsigned& countOfInterations)
{
	double previous = x, following = ((degree - 1) * previous + x / pow(previous, degree)) / degree;
	countOfInterations = 0;

	while (fabs(previous - following) > accuracy)
	{
		previous = following;
		following = ((degree - 1) * previous + x / pow(previous, degree - 1)) / degree;
		countOfInterations++;
	}

	return following;
}